#include <SoftwareSerial.h>
SoftwareSerial wifiSerial(2, 3);  


bool DEBUG = true;   //show more logs
int responseTime = 1000; //communication timeout
int f=0;
// Analog pin 0 will be called 'sensor'
int sensor = 1;
int sensorValue = 0;
int led = 5;// D1(gpio5)
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  wifiSerial.begin(115200);

// -------------------- Start connecting WiFi --------------------------

   while (!wifiSerial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  sendToWifi("AT+RESTORE\r\n",responseTime,DEBUG);
  
  
  sendToWifi("AT+CWMODE=1\r\n",responseTime,DEBUG); // configure as access point
  sendToWifi("AT+CWJAP=\"smart\",\"password\"\r\n",responseTime,DEBUG);
  delay(10000);
  sendToWifi("AT+CIPMUX=1",responseTime,DEBUG); // configure for multiple connections
  sendToWifi("AT+CIFSR",responseTime,DEBUG); // get ip address
  
  sendToWifi("AT+CIPSERVER=1,8082",responseTime,DEBUG); // turn on server on port 80
  
  sendToUno("Wifi connection is running!",responseTime,DEBUG);
  
  pinMode(led,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  
  
  // Read the input on analog pin 0 (named 'sensor')
  sensorValue = analogRead(sensor);
  // Print out the value you read
  Serial.println(sensorValue, DEC);
  // If sensorValue is greater than 500
  if (sensorValue > 500) {
    // Activate digital output pin 8 - the LED will light up
    digitalWrite(led, HIGH);
    sendToWifi("AT+CIPSTART=0,\"TCP\",\"192.168.43.86\",8080,3000",responseTime,DEBUG);
    sendToUno("hi",responseTime,DEBUG);
    sendData("100");
   
    delay(100); 
    
    if(wifiSerial.available())
    {
      sendToUno("hi TCP",responseTime,DEBUG);
      sendData("HI!");
      delay(3000); 
    }
  }
  else {
    // Deactivate digital output pin 8 - the LED will not light up
    digitalWrite(led, LOW);
  }
}  

/*
* Name: sendData
* Description: Function used to send string to tcp client using cipsend
* Params: 
* Returns: void
*/
void sendData(String str){
  String len="";
  len+=str.length();
  sendToWifi("AT+CIPSEND=0,"+len,responseTime,DEBUG);
  delay(100);
  sendToWifi(str,responseTime,DEBUG);
  delay(100);
  sendToWifi("AT+CIPCLOSE=5",responseTime,DEBUG);
}
/*
* Name: sendToWifi
* Description: Function used to send data to ESP8266.
* Params: command - the data/command to send; timeout - the time to wait for a response; debug - print to Serial window?(true = yes, false = no)
* Returns: The response from the esp8266 (if there is a reponse)
*/
String sendToWifi(String command, const int timeout, boolean debug){
  String response = "";
  wifiSerial.println(command); // send the read character to the esp8266
  long int time = millis();
  while( (time+timeout) > millis())
  {
    while(wifiSerial.available())
    {
    // The esp has data so display its output to the serial window 
    char c = wifiSerial.read(); // read the next character.
    response+=c;
    
    }  
  }
 Serial.println(response.length());
  if(response.length()>90&&response.length()<100)
  {
    f=1
    ;
     Serial.println("LENGTH IS VERY HIGH");
 }
  if(debug)
  {
    Serial.println(response);
    
  }
  return response;
}

/*
* Name: sendToWifi
* Description: Function used to send data to ESP8266.
* Params: command - the data/command to send; timeout - the time to wait for a response; debug - print to Serial window?(true = yes, false = no)
* Returns: The response from the esp8266 (if there is a reponse)
*/
String sendToUno(String command, const int timeout, boolean debug){
  String response = "";
  Serial.println(command); // send the read character to the esp8266
  long int time = millis();
  while( (time+timeout) > millis())
  {
    while(Serial.available())
    {
      // The esp has data so display its output to the serial window 
      char c = Serial.read(); // read the next character.
      response+=c;
    }  
  }
  if(debug)
  {
    Serial.println(response);
    
  }
  return response;
}
