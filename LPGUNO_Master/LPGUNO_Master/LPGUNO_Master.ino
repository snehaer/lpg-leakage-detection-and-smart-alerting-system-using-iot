#include <SoftwareSerial.h>
SoftwareSerial wifiSerial(2, 3);  


// defines pins numbers
const int trigPin = 9;
const int echoPin = 10;
const int ledPin = 13;
const int ledPin1 = 12;
 
// defines variables
int f=0;
long duration;
int distance;
int safetyDistance;
String Dist;
int gas_avalue;

int sensorThres=600;

bool DEBUG = true;   //show more logs
int responseTime = 1000; //communication timeout


// ------------------- ## Set up ## -------------------------------------------
void setup() 

{
  //Serial.begin(9600);
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  pinMode(ledPin, OUTPUT);
  pinMode(A1,INPUT);
  //Serial.begin(9600); // Starts the serial communication
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  wifiSerial.begin(115200);

// -------------------- Start connecting WiFi --------------------------

   while (!wifiSerial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  sendToWifi("AT+RESTORE\r\n",responseTime,DEBUG);
  
  
  sendToWifi("AT+CWMODE=1\r\n",responseTime,DEBUG); // configure as access point
  sendToWifi("AT+CWJAP=\"smart\",\"password\"\r\n",responseTime,DEBUG);
  delay(10000);
  sendToWifi("AT+CIPMUX=1",responseTime,DEBUG); // configure for multiple connections
  sendToWifi("AT+CIFSR",responseTime,DEBUG); // get ip address
  
  sendToWifi("AT+CIPSERVER=1,8080",responseTime,DEBUG); // turn on server on port 80
  
  sendToUno("Wifi connection is running!",responseTime,DEBUG);
  
  

}

// ---------------------- Main looping program -----------------------

void loop() 

{
  
  //sendToWifi("AT+CIPSTART=0,\"TCP\",\"192.168.43.1\",8080,5000",responseTime,DEBUG);
  
  if(f==1)
  {
   // Serial.println(f);
  digitalWrite(ledPin1, HIGH); 
   
  }
  else
  {
   digitalWrite(ledPin1, LOW);  
  }
   //digitalWrite(ledPin1, LOW);
  ultra();
  Dist=safetyDistance;
  gas_avalue=analogRead(A1);
  if (gas_avalue >= sensorThres)
  {
    f=0;
    digitalWrite(ledPin, HIGH);
   // digitalWrite(ledPin1, HIGH);
    sendToWifi("AT+CIPSTART=0,\"TCP\",\"192.168.43.1\",8080,5000",responseTime,DEBUG);
    sendToUno("hi",responseTime,DEBUG);
    
    sendData(gas_avalue);
   
    delay(100); 
    
    if(wifiSerial.available())
    {
      sendToUno("hi TCP",responseTime,DEBUG);
      sendData("HI!");
      delay(3000); 
    } 
  }
  
  
  else{
  
    digitalWrite(ledPin, LOW);
    
   //sendToWifi("AT+CIPSTART=0,\"192.168.43.1\",8080,2000",responseTime,DEBUG);
    sendToUno("hi",responseTime,DEBUG);
    sendData(Dist);
    delay(100); 
    
    if(wifiSerial.available())
    {
      sendToUno("hi TCP",responseTime,DEBUG);
      sendData("HI!");
      delay(3000); 
    }
  }
 




 
  if(wifiSerial.available()) // check if the esp is sending a message 
  {
    
      
    if(wifiSerial.find("+IPD,"))
    {
     
     // wait for the serial buffer to fill up (read all the serial data)
     // get the connection id so that we can then disconnect
     int connectionId = wifiSerial.read()-48; // subtract 48 because the read() function returns      
     Serial.println("connection="+connectionId);                                      // the ASCII decimal value and 0 (the first decimal number) starts at 48
          
     //wifiSerial.find("pin="); // advance cursor to "p="
         
    // int pinNumber = (wifiSerial.read()-48); // get first number i.e. if the pin 13 then the 1st number is 1
     //Serial.println("pin="+pinNumber);
     //if(pinNumber==2){
      // digitalWrite(ledPin1, HIGH); //FORWARD
       //digitalWrite(ledPin2, HIGH);
        //}
        String closeCommand = "AT+CIPCLOSE="; 
     closeCommand+=connectionId; // append connection id
     closeCommand+="\r\n";
     
     
     sendToWifi(closeCommand,2000,DEBUG);
     sendToWifi("AT+CIPBUFRESET\r\n",2000,DEBUG);
    
     
    }
  }

// -------------- WiFi : Get order from client -----------------

  
}

// ---------------------- Sub function ------------------------------



void ultra()
{
  
  // Clears the trigPin
digitalWrite(trigPin, LOW);
delayMicroseconds(2);
 
// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPin, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin, LOW);
 
// Reads the echoPin, returns the sound wave travel time in microseconds
duration = pulseIn(echoPin, HIGH);
 
// Calculating the distance
distance= duration*0.034/2;
 
safetyDistance = distance;  
}


/*
* Name: sendData
* Description: Function used to send string to tcp client using cipsend
* Params: 
* Returns: void
*/
void sendData(String str){
  String len="";
  len+=str.length();
  sendToWifi("AT+CIPSEND=0,"+len,responseTime,DEBUG);
  delay(100);
  sendToWifi(str,responseTime,DEBUG);
  delay(100);
  sendToWifi("AT+CIPCLOSE=5",responseTime,DEBUG);
}
/*
* Name: sendToWifi
* Description: Function used to send data to ESP8266.
* Params: command - the data/command to send; timeout - the time to wait for a response; debug - print to Serial window?(true = yes, false = no)
* Returns: The response from the esp8266 (if there is a reponse)
*/
String sendToWifi(String command, const int timeout, boolean debug){
  String response = "";
  wifiSerial.println(command); // send the read character to the esp8266
  long int time = millis();
  while( (time+timeout) > millis())
  {
    while(wifiSerial.available())
    {
    // The esp has data so display its output to the serial window 
    char c = wifiSerial.read(); // read the next character.
    response+=c;
    
    }  
  }
 Serial.println(response.length());
  if(response.length()>90&&response.length()<100)
  {
    if(f==0)
    {
     f=1 ; 
    }
    else
    {
      f=0;
    }
    
     Serial.println("LENGTH IS VERY HIGH");
 }
  if(debug)
  {
    Serial.println(response);
    
  }
  return response;
}

/*
* Name: sendToWifi
* Description: Function used to send data to ESP8266.
* Params: command - the data/command to send; timeout - the time to wait for a response; debug - print to Serial window?(true = yes, false = no)
* Returns: The response from the esp8266 (if there is a reponse)
*/
String sendToUno(String command, const int timeout, boolean debug){
  String response = "";
  Serial.println(command); // send the read character to the esp8266
  long int time = millis();
  while( (time+timeout) > millis())
  {
    while(Serial.available())
    {
      // The esp has data so display its output to the serial window 
      char c = Serial.read(); // read the next character.
      response+=c;
    }  
  }
  if(debug)
  {
    Serial.println(response);
    
  }
  return response;
}
