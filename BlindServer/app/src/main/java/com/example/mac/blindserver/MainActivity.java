package com.example.mac.blindserver;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class MainActivity extends AppCompatActivity implements SensorEventListener, StepListener {

    TextView info, infoip, msg;
    String message = "";
    ServerSocket serverSocket;
    TextToSpeech t1;
    Button btnSend;
    Button btnStart,btnStop;
    ToggleButton tgButton;
    EditText txtPhone;

    private TCPClient mTcpClient;

    private StepDetector simpleStepDetector;
    private SensorManager sensorManager;
    private Sensor accel;
    private static final String TEXT_NUM_STEPS = "Number of Steps: ";
    private int numSteps;
    private WebView mWebview ;
    int TankVal=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get an instance of the SensorManager
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        simpleStepDetector = new StepDetector();
        simpleStepDetector.registerListener(this);

        mWebview  = new WebView(this);

        info = (TextView) findViewById(R.id.info);
        infoip = (TextView) findViewById(R.id.infoip);
        msg = (TextView) findViewById(R.id.msg);

        infoip.setText(getIpAddress());




        Thread socketServerThread = new Thread(new SocketServerThread());
        socketServerThread.start();

        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });

        //ON
      
//OFF

//Configuration


    }

    private boolean sendMessage(final String msg, final String dstIp, final int dstPort) {

        DatagramSocket sendSocket = null;

        try {
            sendSocket = new DatagramSocket();

            final InetAddress local = InetAddress.getByName(dstIp);
            final int msg_length = msg.length();
            final byte[] message1 = msg.getBytes();

            final DatagramPacket sendPacket = new DatagramPacket(message1,
                    msg_length, local, dstPort);
            sendSocket.send(sendPacket);



        } catch (final Exception e) {
            e.printStackTrace();
            return false;
        } finally {

            if (sendSocket != null) {
                sendSocket.disconnect();
                sendSocket.close();
                sendSocket = null;
            }
        }
        return true;
    }
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            simpleStepDetector.updateAccel(
                    event.timestamp, event.values[0], event.values[1], event.values[2]);
        }
    }

    @Override
    public void step(long timeNs) {
        numSteps++;
        msg.setText(TEXT_NUM_STEPS + numSteps);

        if(numSteps==20){

            new Thread(new Runnable() {
                @Override
                public void run() {
                    OutputStream out2;
                    try {



                        Socket socket = new Socket("192.168.43.173",8082);
                        out2 = socket.getOutputStream();
                        PrintStream printStream = new PrintStream(out2);
                        printStream.print("pin=2");
                        printStream.close();




                    } catch (Exception ex) {
                    }

                }
            }).start();

        }
        else if(numSteps==45){

            new Thread(new Runnable() {
                @Override
                public void run() {
                    OutputStream out2;
                    try {



                        Socket socket = new Socket("192.168.43.233",8081);
                        out2 = socket.getOutputStream();
                        PrintStream printStream = new PrintStream(out2);
                        printStream.print("pin=2");
                        printStream.close();





                    } catch (Exception ex) {
                    }

                }
            }).start();

        }
        else if(numSteps==75){

            new Thread(new Runnable() {
                @Override
                public void run() {
                    OutputStream out2;
                    try {



                        Socket socket = new Socket("192.168.43.233",8081);
                        out2 = socket.getOutputStream();
                        PrintStream printStream = new PrintStream(out2);
                        printStream.print("pin=2");
                        printStream.close();





                    } catch (Exception ex) {
                    }

                }
            }).start();

        }
        else  if(numSteps==115){

            new Thread(new Runnable() {
                @Override
                public void run() {
                    OutputStream out2;
                    try {



                        Socket socket = new Socket("192.168.43.173",8082);
                        out2 = socket.getOutputStream();
                        PrintStream printStream = new PrintStream(out2);
                        printStream.print("pin=2");
                        printStream.close();




                    } catch (Exception ex) {
                    }

                }
            }).start();

        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    //needed

    private class SocketServerThread extends Thread {

        static final int SocketServerPORT = 8080;
        int count = 0;

        @Override
        public void run() {
            try {
                serverSocket = new ServerSocket(SocketServerPORT);
                MainActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        info.setText("I'm waiting here: "
                                + serverSocket.getLocalPort());
                    }
                });

                while (true) {
                    Socket socket = serverSocket.accept();
                    count++;
                    message += "#" + count + " from " + socket.getInetAddress()
                            + ":" + socket.getPort() + "\n";

                    MainActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            msg.setText(message);
                        }
                    });

                    SocketServerReplyThread socketServerReplyThread = new SocketServerReplyThread(
                            socket, count);
                    socketServerReplyThread.run();

                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    private class SocketServerBtnThread extends Thread {

        static final int SocketServerPORT = 8081;
        int count = 0;

        @Override
        public void run() {
            try {
                serverSocket = new ServerSocket(SocketServerPORT);
                MainActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        info.setText("I'm waiting here: "
                                + serverSocket.getLocalPort());
                    }
                });

                while (true) {
                    Socket socket = serverSocket.accept();
                    count++;
                    message += "#" + count + " from " + socket.getInetAddress()
                            + ":" + socket.getPort() + "\n";

                    MainActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            msg.setText(message);
                        }
                    });

                    SocketServerSendThread socketServersendThread = new SocketServerSendThread(
                            socket, 1);
                    socketServersendThread.run();

                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    //needed
    private class SocketServerReplyThread extends Thread {

        private Socket hostThreadSocket;
        int cnt;

        SocketServerReplyThread(Socket socket, int c) {
            hostThreadSocket = socket;
            cnt = c;
        }
        String data="";
        String dataPlay="";

        @Override
        public void run() {
            OutputStream outputStream;
            String msgReply = "Hello from Android, you are #" + cnt;

            try {
                outputStream = hostThreadSocket.getOutputStream();
                PrintStream printStream = new PrintStream(outputStream);
                printStream.print(msgReply);

                InputStream inputStream=hostThreadSocket.getInputStream();
                final int bufferSize = 1024;
                final char[] buffer = new char[bufferSize];
                final StringBuilder out = new StringBuilder();
                Reader in = new InputStreamReader(inputStream, "UTF-8");
                for (; ; ) {
                    int rsz = in.read(buffer, 0, buffer.length);
                    if (rsz < 0)
                        break;
                    out.append(buffer, 0, rsz);
                }

                data= out.toString();
                printStream.close();


                //message += "replayed: " + msgReply + "\n";
                //message="Object Found";

                MainActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {


                        //TankVal = Integer.valueOf(data) ;//TankVal+1;
                        //int TankSpeak=TankVal*1;


                        msg.setText("Gas Leakage");
                        t1.speak("Gas Leakage  Detected", TextToSpeech.QUEUE_FLUSH, null);
                        sendSMSMessage();
                        //message=String.valueOf(TankSpeak)+"%";
                        //SmsManager smsManager = SmsManager.getDefault();
                        //smsManager.sendTextMessage("9995885561", null, "sms message", null, null);

                        if(TankVal==10){TankVal=0;}
                    }
                });

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                message += "Something wrong! " + e.toString() + "\n";
            }

            MainActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    msg.setText(message);
                }
            });
        }

    }
    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS =0 ;
    protected void sendSMSMessage() {



        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.SEND_SMS)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.SEND_SMS},
                        MY_PERMISSIONS_REQUEST_SEND_SMS);
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    SmsManager smsManager = SmsManager.getDefault();
                    txtPhone=(EditText)findViewById(R.id.editCapacity);
                    smsManager.sendTextMessage(txtPhone.getText().toString(), null, "Gas Leakage", null, null);
                    Toast.makeText(getApplicationContext(), "SMS sent.",
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "SMS faild, please try again.", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }

    }

    private class SocketServerSendThread extends Thread {

        private Socket hostThreadSocket;
        int cnt;

        SocketServerSendThread(Socket socket, int c) {
            hostThreadSocket = socket;
            cnt = c;
        }

        @Override
        public void run() {
            OutputStream outputStream;
            String msgReply = "Hello from Android, you are #" + cnt;
            if(cnt==1){
                msgReply="Left";
            }
            else{msgReply="Right";}


            try {
                outputStream = hostThreadSocket.getOutputStream();
                PrintStream printStream = new PrintStream(outputStream);
                printStream.print(msgReply);
                printStream.close();
                message += "replayed: " + msgReply + "\n";
                message="Object Found";

                MainActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        msg.setText("Object Found");
                        t1.speak("Object Found", TextToSpeech.QUEUE_FLUSH, null);
                    }
                });

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                message += "Something wrong! " + e.toString() + "\n";
            }

            MainActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    msg.setText(message);
                }
            });
        }

    }


    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += "SiteLocalAddress: "
                                + inetAddress.getHostAddress() + "\n";
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }


}

