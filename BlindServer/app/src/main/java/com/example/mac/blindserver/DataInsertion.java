package com.example.mac.blindserver;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Melvin on 17/03/16.
 */





public class DataInsertion extends AsyncTask<String, Void, String> {


    private int responseHttp = 0;
    private String flag="false";


    @Override
    protected String doInBackground(String... urltest) {
        // TODO Auto-generated method stub
        try {
            URL url = new URL(urltest[0]);
            URLConnection connection = url.openConnection();
            connection.setConnectTimeout(8000);
            HttpURLConnection httpConnection = (HttpURLConnection) connection;
            httpConnection.setConnectTimeout(8000);
            responseHttp = httpConnection.getResponseCode();

            InputStream inputStream = httpConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
            String response = "";
            String line = "";

            while ((line = bufferedReader.readLine())!=null)
            {
                response+= line;

            }

            if (responseHttp == HttpURLConnection.HTTP_OK) {
                flag = response;
            } else {
                flag = "false";
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("My Error is: "+e.toString());
        }
        return flag;
    }

    @Override
    protected void onPostExecute(String recieve) {
        if(recieve.equalsIgnoreCase("true"))
        {

            Log.i("MySQL", "DATA is working");
        }else if(recieve.equalsIgnoreCase("false"))
        {
            Log.i("MySQL", "DATA is Not working");
        }
    }


}
