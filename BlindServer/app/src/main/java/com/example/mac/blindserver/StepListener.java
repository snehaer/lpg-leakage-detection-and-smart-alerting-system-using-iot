package com.example.mac.blindserver;

public interface StepListener {
    public void step(long timeNs);
}
